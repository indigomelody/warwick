import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import cheerio from "cheerio-without-node-native";
import Swiper from "react-native-swiper";
import * as rssParser from "react-native-rss-parser";
import Lightbox from "react-native-lightbox";
import _ from "lodash";

import Overlay from "./Overlay";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      comments: [],
      overlayContent: null
    };
  }

  componentDidMount() {
    this.getAllExperts().then(experts => {
      fetch(
        "https://warwick.ac.uk/sitebuilder2/api/rss/news.rss?page=/newsandevents/expertcomment"
      )
        .then(response => response.text())
        .then(responseData => rssParser.parse(responseData))
        .then(rss => {
          this.matchExperts(experts, rss.items.slice(0, 5)).then(comments => {
            this.matchFullStories(comments).then(sortedExpandedStatements => {
              this.setState({
                comments: sortedExpandedStatements
              });
            });
          });
        })
        .catch(error => {
          console.error(error);
        });
    });
  }

  getAllExperts() {
    return new Promise((resolve, reject) => {
      let experts = [];
      let totalExperts = 900;
      for (let i = 0; i * 100 <= totalExperts; i++) {
        fetch(
          `https://experts.warwick.ac.uk/results.json?searchTerm=*&page=${i}&size=100`
        )
          .then(response => response.json())
          .then(responseJson => {
            experts = [...experts, ...responseJson.experts];
            totalExperts = responseJson.totalHits;
            if (experts.length === totalExperts) {
              resolve(experts);
            }
            return true;
          })
          .catch(error => {
            console.error(error);
          });
      }
    });
  }

  findExpert(experts, comment) {
    return new Promise((resolve, reject) => {
      experts.map((expert, i) => {
        const nameToSearch = this.trimTitles(
          this.stripTags(expert[1].value.toLowerCase())
        );
        if (
          comment.title.toLowerCase().search(nameToSearch) >= 0 ||
          comment.description.toLowerCase().search(nameToSearch) >= 0
        ) {
          resolve({ expert: nameToSearch });
          return true;
        }
        if (i + 1 === experts.length) {
          reject();
        }
      });
    });
  }

  fetchExpertId(expertName) {
    return new Promise((resolve, reject) =>
      fetch(
        `https://experts.warwick.ac.uk/results.json?searchTerm=${expertName}*&page=0&size=1`
      )
        .then(response => response.json())
        .then(responseJson => {
          resolve(responseJson.experts[0][0].value);
        })
        .catch(() => {
          reject();
        })
    );
  }

  getExpert(expertName) {
    return new Promise((resolve, reject) => {
      this.fetchExpertId(expertName)
        .then(id => {
          const expertID = id;

          return fetch(
            `https://experts.warwick.ac.uk/profile?expert=${expertID}&stat_id=*`,
            {
              method: "GET",
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
              }
            }
          )
            .then(response => response.json())
            .then(responseJson => {
              resolve(responseJson);
            })
            .catch(error => {
              reject();
              console.error(error);
            });
        })
        .catch(() => {
          reject();
        });
    });
  }

  matchExperts(expertSet, comments) {
    return new Promise((resolve, reject) => {
      const newComments = [];
      comments.map((individualComment, index) => {
        const comment = individualComment;
        comment.index = index;
        this.findExpert(expertSet, comment)
          .then(expert => {
            this.getExpert(expert.expert)
              .then(fullExpert => {
                const updatedComment = comment;
                updatedComment.author = fullExpert;
                newComments.push(updatedComment);
                if (newComments.length === comments.length) {
                  resolve(newComments);
                }
                return updatedComment;
              })
              .catch(() => {
                newComments.push(comment);
                if (newComments.length === comments.length) {
                  resolve(newComments);
                }
              });
          })
          .catch(() => {
            newComments.push(comment);
            if (newComments.length === comments.length) {
              resolve(newComments);
            }
          });
      });
    });
  }

  matchFullStories(comments) {
    return new Promise((resolve, reject) => {
      const expandedStories = [];
      comments.map(comment => {
        this.scrapeStory(comment).then(expandedComment => {
          const newComment = comment;
          newComment.body = expandedComment;
          expandedStories.push(newComment);
          if (expandedStories.length === comments.length) {
            resolve(_.orderBy(expandedStories, ["index"], ["asc"]));
          }
        });
      });
    });
  }

  async scrapeStory(story) {
    const data = await fetch(story.links[0].url);
    const $ = cheerio.load(await data.text());
    const allText = $(".column-1-content p")
      .get()
      .map(repo => {
        const $repo = $(repo);
        const title = $repo.text();
        return title;
      });
    return allText;
  }

  trimTitles(value) {
    return value.replace(/dr |mr |professor |miss |mrs/gi, "");
  }

  stripTags(val) {
    return val.replace(/<(?:.|\n)*?>/gm, "").replace(/&quot;/g, '"');
  }

  trimIf(val, length) {
    if (val.length > length) {
      return `${val.substr(0, length)}...`;
    }
    return val;
  }

  render() {
    const { comments, lightboxOpen } = this.state;
    if (!comments.length) {
      return <View />;
    }
    const general = StyleSheet.create({
      row: {
        flexDirection: "row",
        backgroundColor: "#fff",
        padding: 10,
        marginTop: 10,
        flex: 1
      },
      header: {
        fontSize: 18,
        color: "#79991B",
        fontWeight: "bold",
        textAlign: "center"
      }
    });

    const commentsBox = StyleSheet.create({
      wrapper: { height: 210 },
      slides: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        paddingLeft: 25,
        paddingRight: 25,
        paddingBottom: 20
      },
      title: {
        color: "#444",
        fontSize: 16,
        lineHeight: 20,
        fontWeight: "bold",
        alignSelf: "stretch"
      },
      comment: {
        color: "#444",
        fontSize: 13,
        marginTop: 5,
        alignSelf: "stretch"
      },
      titleOpen: {
        color: "#444",
        fontSize: 18,
        lineHeight: 20,
        fontWeight: "bold",
        alignSelf: "stretch"
      },
      commentOpen: {
        color: "#444",
        fontSize: 15,
        marginTop: 15,
        alignSelf: "stretch"
      },
      buttonTextLeft: {
        color: "#eee",
        fontSize: 30,
        marginTop: -15,
        marginLeft: -10
      },
      buttonTextRight: {
        color: "#eee",
        fontSize: 30,
        marginTop: -15,
        marginRight: -10
      },
      btn: {
        color: "#204F79",
        fontWeight: "bold",
        marginTop: 10
      }
    });

    const { overlayContent } = this.state;

    return (
      <View style={general.row}>
        {overlayContent && (
          <Overlay
            content={overlayContent}
            closeOverlay={() => {
              this.setState({ overlayContent: null });
            }}
          />
        )}
        <View
          style={{
            flex: 1
          }}
        >
          <Text style={general.header}>Latest expert comments</Text>
          <Swiper
            style={commentsBox.wrapper}
            showsButtons
            dotColor="#eeeeee"
            activeDotColor="#79991B"
            paginationStyle={{ marginBottom: -15 }}
            nextButton={<Text style={commentsBox.buttonTextRight}>›</Text>}
            prevButton={<Text style={commentsBox.buttonTextLeft}>‹</Text>}
          >
            {comments.map(comment => (
              <View key={comment.id} style={commentsBox.slides}>
                <Text style={commentsBox.title}>
                  {this.trimIf(this.stripTags(comment.title), 100)}
                </Text>
                <Text style={commentsBox.comment}>
                  {this.trimIf(this.stripTags(comment.description), 160)}
                </Text>

                <Text
                  onPress={() => {
                    this.setState({ overlayContent: comment });
                  }}
                  style={commentsBox.btn}
                >
                  click here to read more
                </Text>
              </View>
            ))}
          </Swiper>
        </View>
      </View>
    );
  }
}
