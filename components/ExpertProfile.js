import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import _ from "lodash";

import { Button, Icon } from "react-native-elements";
import Communications from "react-native-communications";

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  removeCodes(number) {
    const trimmed = number.replace(/\s/g, "").replace(/[{()}]/g, "");
    const firstTwo = trimmed.substring(0, 2);
    const firstThree = trimmed.substring(0, 3);
    const firstFour = trimmed.substring(0, 4);

    if (firstTwo === "44" || firstThree === "+44") {
      switch (true) {
        case firstThree === "440":
          return trimmed.substring(2, number.length);

        case firstTwo === "44":
          return "0" + trimmed.substring(2, number.length);

        case firstFour === "+440":
          return trimmed.substring(3, number.length);

        case firstThree === "+44":
          return "0" + trimmed.substring(3, number.length);
      }
    }

    return number;
  }

  render() {
    const item = this.props.expert;
    return (
      <View style={[views.card, { paddingBottom: 18 }]}>
        <View style={{ flexDirection: "row" }}>
          {/* <Image
            source={{
              uri: `https://experts.warwick.ac.uk/${item[0].value}/photo.jpg`,
            }}
            style={{
              width: 100,
              height: 100,
            }}
          /> */}
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              paddingLeft: 10,
              paddingRight: 10
            }}
          >
            <Text
              style={{
                fontSize: 18,
                color: "#79991B",
                paddingRight: 20,
                marginTop: 10
              }}
            >
              {item.title} {item.firstName} {item.surname}
            </Text>
            {/* <Text style={{ fontSize: 17, color: '#777', marginTop: 2 }}>
              {this.stripTags(
                _.find(item, row => row.fieldName === 'departmentName').value,
              )}
            </Text> */}
          </View>
        </View>
        <View
          style={{
            paddingLeft: 15,
            paddingRight: 15,
            marginTop: 10,
            paddingBottom: 20
          }}
        >
          <Text style={{ fontSize: 16, color: "#79991B", marginTop: 15 }}>
            Email
          </Text>
          <Text
            style={{ fontSize: 16, color: "#204F79" }}
            onPress={() => {
              Communications.email(item.emailAddresses, "", "", "Enquiry", "");
            }}
          >
            {item.emailAddresses}
          </Text>
          <Text style={{ fontSize: 16, color: "#79991B", marginTop: 15 }}>
            Office Number
          </Text>
          <Text
            style={{ fontSize: 16, color: "#204F79", marginTop: 2 }}
            onPress={() => {
              Communications.phonecall(
                this.removeCodes(item.officeTelephone),
                false
              );
            }}
          >
            {this.removeCodes(item.officeTelephone)}
          </Text>
          <Text style={{ fontSize: 16, color: "#79991B", marginTop: 15 }}>
            Contact Press Office
          </Text>
          <Text
            style={{ fontSize: 16, color: "#204F79", marginTop: 2 }}
            onPress={() => {
              Communications.phonecall(
                this.removeCodes(item.alternativeOfficeTelephone),
                false
              );
            }}
          >
            {this.removeCodes(item.alternativeOfficeTelephone)}
          </Text>
          <Text style={{ fontSize: 16, color: "#79991B", marginTop: 15 }}>
            Spoken Languages
          </Text>
          <Text style={{ fontSize: 16, marginTop: 3, color: "#555" }}>
            {item.languagesSpoken.map((language, index) => {
              if (index + 1 === item.languagesSpoken.length) {
                return language;
              }
              return `${language}, `;
            })}
          </Text>
        </View>
        <Button
          raised
          style={{
            flex: 1
          }}
          icon={{
            name: "phone",
            type: "font-awesome",
            color: "#fff"
          }}
          onPress={() => {
            Communications.phonecall(
              this.removeCodes(item.officeTelephone),
              false
            );
          }}
          backgroundColor="#204F79"
          color="#fff"
          fontSize={16}
          title="Call Expert"
        />
      </View>
    );
  }
}

const views = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E1E1E1",
    //    alignItems: "center",
    justifyContent: "center",
    position: "relative",
    padding: 10
  },
  header: {
    flexDirection: "row",
    paddingTop: 40
  },
  searchBox: {
    backgroundColor: "#79991B",
    padding: 10
  },
  card: {
    flex: 1,
    padding: 10,
    marginTop: 10,
    backgroundColor: "#ffffff"
  },
  name: {
    paddingBottom: 5
  },
  department: {
    paddingBottom: 10
  }
});

const texts = StyleSheet.create({
  inputField: {
    color: "red"
  },
  name: {
    color: "#79991B",
    fontSize: 18
  },
  department: {
    color: "#555555",
    fontSize: 16
  },
  expertise: {
    color: "#999999",
    fontSize: 14
  }
});
