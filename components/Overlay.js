import React from "react";
import _ from "lodash";
import { Dimensions, StyleSheet, Text, View, ScrollView } from "react-native";
import { Overlay, Button, Icon } from "react-native-elements";
import ExpertProfile from "./ExpertProfile";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: [],
      comment: this.props.content,
      isVisible: true
    };
  }

  stripTags(val) {
    return val.replace(/<(?:.|\n)*?>/gm, "").replace(/&quot;/g, '"');
  }

  render() {
    const { comment, isVisible } = this.state;

    const commentsBox = StyleSheet.create({
      title: {
        color: "#444",
        fontSize: 16,
        lineHeight: 20,
        fontWeight: "bold",
        alignSelf: "stretch"
      },
      titleOpen: {
        color: "#444",
        fontSize: 18,
        lineHeight: 20,
        fontWeight: "bold",
        alignSelf: "stretch",
        paddingBottom: 15
      },
      comment: {
        color: "#444",
        fontSize: 13,
        marginBottom: 5,
        alignSelf: "stretch"
      },
      commentOpen: {
        color: "#444",
        fontSize: 15,
        marginBottom: 15,
        alignSelf: "stretch"
      }
    });

    return (
      <Overlay isVisible={isVisible} fullScreen>
        <View
          style={{
            flexDirection: "row",
            backgroundColor: "#fff",
            padding: 10,
            marginTop: 50,
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Icon
            onPress={() => this.props.closeOverlay()}
            name="times"
            type="font-awesome"
            raised
            size={20}
            color="#79991B"
          />
        </View>

        <View style={{ paddingTop: 10 }}>
          <ScrollView height={Dimensions.get("window").height - 200}>
            <Text style={commentsBox.titleOpen}>
              {this.stripTags(comment.title)}
            </Text>
            {comment.body.map(paragraph => (
              <Text key={paragraph} style={commentsBox.commentOpen}>
                {this.stripTags(paragraph)}
              </Text>
            ))}
            <View
              style={{
                marginTop: 30,
                borderTopWidth: 1,
                borderColor: "#777777"
              }}
            >
              {comment.author && <ExpertProfile expert={comment.author} />}
            </View>
          </ScrollView>
        </View>
      </Overlay>
    );
  }
}
