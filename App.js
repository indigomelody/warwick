import React from "react";
import {
  StyleSheet,
  FlatList,
  ActivityIndicator,
  Text,
  TextInput,
  View,
  Image,
  Dimensions,
  Keyboard,
  TouchableWithoutFeedback
} from "react-native";
import { Button, Icon } from "react-native-elements";
import _ from "lodash";
import Communications from "react-native-communications";

import CommentsBox from "./components/CommentsBox";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      searchPage: 0,
      searchTerm: "",
      placeholder: "Enter a search term...",
      showingFilters: false,
      filters: [
        { name: "Name", status: true, term: "name" },
        { name: "Role", status: true, term: "title" },
        { name: "Department", status: true, term: "departmentName" },
        { name: "Expertise", status: true, term: "subjects" },
        { name: "Languages", status: true, term: "languagesSpoken" }
      ],
      totalHits: 0,
      firstShow: true
    };
  }

  componentDidMount() {
    return fetch(
      `https://experts.warwick.ac.uk/results.json?searchTerm=*&page=${
        this.state.searchPage
      }${this.getFilterString()}&size=10&col%5B1%5D=1&col%5B2%5D=1`
    )
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          isLoading: false,
          dataSource: responseJson.experts,
          totalHits: responseJson.totalHits
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  getFilterString() {
    let filterString = "";
    let count = 0;
    this.state.filters.map(filter => {
      if (filter.status) {
        count = count + 1;
        filterString += `&searchFields=${filter.term}`;
      }
    });
    if (count === this.state.filters.length) {
      return "";
    }
    return filterString;
  }

  getMoreContent() {
    return fetch(
      `https://experts.warwick.ac.uk/results.json?searchTerm=${this.state
        .searchTerm || ""}*${this.getFilterString()}&page=${this.state
        .searchPage + 1}&size=100&col%5B1%5D=1&col%5B2%5D=1`
    )
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          searchPage: this.state.searchPage + 1,
          dataSource: [...this.state.dataSource, ...responseJson.experts]
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  search() {
    Keyboard.dismiss();
    return fetch(
      `https://experts.warwick.ac.uk/results.json?searchTerm=${this.state
        .searchTerm ||
        ""}*${this.getFilterString()}&page=0&size=10&col%5B1%5D=1&col%5B2%5D=1`
    )
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          dataSource: responseJson.experts,
          totalHits: responseJson.totalHits,
          showingFilters: false,
          searchPage: 0,
          firstShow: false
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  toggleExpert(expert) {
    if (expert.expand) {
      const pos = _.findIndex(this.state.dataSource, exp => {
        return exp[0].value === expert[0].value;
      });
      const experts = this.state.dataSource;
      experts[pos].expand = false;

      this.setState({
        dataSource: experts
      });
    } else {
      this.getExpert(expert);
    }
  }

  getExpert(expert) {
    const expertID = expert[0].value;

    return fetch(
      `https://experts.warwick.ac.uk/profile?expert=${expertID}&stat_id=*`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      }
    )
      .then(response => response.json())
      .then(responseJson => {
        const pos = _.findIndex(this.state.dataSource, expert => {
          return expert[0].value === expertID;
        });
        const experts = this.state.dataSource;
        experts[pos].data = responseJson;
        experts[pos].expand = true;
        this.setState({
          dataSource: experts
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  stripTags(val) {
    return val.replace(/<(?:.|\n)*?>/gm, "");
  }

  trimIf(val, length) {
    if (val.length > length) {
      return val.substr(0, length) + "...";
    }
    return val;
  }

  removeCodes(number) {
    let trimmed = number.replace(/\s/g, "").replace(/[{()}]/g, "");
    let firstTwo = trimmed.substring(0, 2);
    let firstThree = trimmed.substring(0, 3);
    let firstFour = trimmed.substring(0, 4);

    if (firstTwo === "44" || firstThree === "+44") {
      switch (true) {
        case firstThree === "440":
          return trimmed.substring(2, number.length);

        case firstTwo === "44":
          return "0" + trimmed.substring(2, number.length);

        case firstFour === "+440":
          return trimmed.substring(3, number.length);

        case firstThree === "+44":
          return "0" + trimmed.substring(3, number.length);
      }
    }

    return number;
  }

  header() {
    return (
      <View style={views.header}>
        <Image
          source={require("./img/header.png")}
          style={{
            flex: 1,
            width: null,
            height: null,
            aspectRatio: 710 / 290
          }}
          resizeMode="cover"
        />
      </View>
    );
  }

  introText() {
    return (
      <View
        style={{
          flexDirection: "row",
          backgroundColor: "#fff",
          padding: 10,
          marginTop: 10,
          flex: 1
        }}
      >
        <View
          style={{
            flex: 1
          }}
        >
          <Text
            style={{
              fontSize: 16,
              color: "#555",
              fontWeight: "bold",
              textAlign: "center"
            }}
          >
            The Press Office often has additional means of contacting
            researchers including their mobile and out-of-hours numbers. Please
            contact us if we can help. Email{" "}
            <Text
              style={{ fontSize: 16, color: "#204F79", marginTop: 2 }}
              onPress={() => {
                Communications.email(
                  "press@warwick.ac.uk",
                  "",
                  "",
                  "Enquiry",
                  ""
                );
              }}
            >
              press@warwick.ac.uk
            </Text>{" "}
            or call{" "}
            <Text
              style={{ fontSize: 16, color: "#204F79", marginTop: 2 }}
              onPress={() => {
                Communications.phonecall("02476524668", false);
              }}
            >
              +44 24 7652 4668
            </Text>
            . For urgent or out-of-hours assistance call{" "}
            <Text
              style={{ fontSize: 16, color: "#204F79", marginTop: 2 }}
              onPress={() => {
                Communications.phonecall("07767655860", false);
              }}
            >
              +44 7767 655860
            </Text>{" "}
            or{" "}
            <Text
              style={{ fontSize: 16, color: "#204F79", marginTop: 2 }}
              onPress={() => {
                Communications.phonecall("07785433155", false);
              }}
            >
              +44 (0) 7785 433155
            </Text>
            .
          </Text>
        </View>
      </View>
    );
  }

  searchBox() {
    return (
      <View style={views.searchBox}>
        <TextInput
          style={{
            height: 40,
            backgroundColor: "#ffffff",
            padding: 10,
            fontSize: 18
          }}
          underlineColorAndroid="rgba(0,0,0,0)"
          onChangeText={text => this.setState({ searchTerm: text })}
          value={this.state.searchTerm}
          placeholder={this.state.placeholder}
        />
      </View>
    );
  }
  searchButtons() {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "flex-end",
          backgroundColor: "#79991B",
          paddingBottom: 10
        }}
      >
        <View style={{ margin: 0 }}>
          <Button
            raised
            icon={{
              name: "filter",
              type: "font-awesome",
              color: "#555555",
              size: 16
            }}
            onPress={() => {
              this.setState({
                showingFilters: !this.state.showingFilters
              });
            }}
            containerStyle={{
              marginRight: 10
            }}
            buttonStyle={{
              backgroundColor: "#EBEBEB"
            }}
            title="Select Filters"
            titleStyle={{
              color: "#555555",
              fontSize: 16
            }}
          />
        </View>
        <View>
          <Button
            raised
            icon={{
              name: "search",
              type: "font-awesome",
              color: "#fff",
              size: 16
            }}
            onPress={() => {
              this.search();
            }}
            title="Search"
            containerStyle={{
              marginRight: 10
            }}
            buttonStyle={{
              backgroundColor: "#204F79"
            }}
            titleStyle={{
              color: "#fff",
              fontSize: 16
            }}
          />
        </View>
      </View>
    );
  }

  filters() {
    return (
      <View
        style={{
          flexDirection: "row",
          backgroundColor: "#fff",
          padding: 10
        }}
      >
        <FlatList
          numColumns={2}
          style={{ flexDirection: "column" }}
          data={this.state.filters}
          renderItem={({ item, index }) => (
            <TouchableWithoutFeedback
              onPress={() => {
                const { filters } = this.state;
                filters[index].status = !filters[index].status;
                this.setState({
                  filters
                });
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  width: (Dimensions.get("window").width - 40) / 2
                }}
              >
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon
                    name="check"
                    type="font-awesome"
                    reverse
                    raised
                    size={16}
                    color={item.status ? "#79991B" : "#E1E1E1"}
                  />
                  <Text style={{ color: "#555", marginLeft: 10, fontSize: 16 }}>
                    {item.name}
                  </Text>
                </View>
              </View>
            </TouchableWithoutFeedback>
          )}
          keyExtractor={(item, index) => `filter-item-${index}`}
        />
      </View>
    );
  }

  showExpertsCount() {
    return (
      <View
        style={{
          flexDirection: "row",
          backgroundColor: "#fff",
          padding: 10,
          marginTop: 10,
          flex: 1
        }}
      >
        <View
          style={{
            flex: 1
          }}
        >
          <Text style={{ fontSize: 16, color: "#777777", textAlign: "center" }}>
            {this.state.totalHits} Experts
          </Text>
        </View>
      </View>
    );
  }

  card(item) {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          this.toggleExpert(item);
        }}
      >
        {item.expand ? (
          <View style={[views.card, { paddingBottom: 18 }]}>
            <View style={{ position: "absolute", right: 20, top: 10 }}>
              <Icon
                name="chevron-up"
                type="font-awesome"
                size={16}
                color="#E1E1E1"
              />
            </View>
            <View style={{ flexDirection: "row" }}>
              <Image
                source={{
                  uri: `https://experts.warwick.ac.uk/${
                    item[0].value
                  }/photo.jpg`
                }}
                style={{
                  width: 100,
                  height: 100
                }}
              />
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  paddingLeft: 10,
                  paddingRight: 10
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    color: "#79991B",
                    paddingRight: 20,
                    marginTop: 10
                  }}
                >
                  {item.data.title} {item.data.firstName} {item.data.surname}
                </Text>
                <Text style={{ fontSize: 17, color: "#777", marginTop: 2 }}>
                  {this.stripTags(
                    _.find(item, row => row.fieldName === "departmentName")
                      .value
                  )}
                </Text>
              </View>
            </View>
            <View
              style={{
                paddingLeft: 15,
                paddingRight: 15,
                marginTop: 10,
                paddingBottom: 20
              }}
            >
              <Text style={{ fontSize: 16, color: "#79991B", marginTop: 15 }}>
                Email
              </Text>
              <Text
                style={{ fontSize: 16, color: "#204F79" }}
                onPress={() => {
                  Communications.email(
                    item.data.emailAddresses,
                    "",
                    "",
                    "Enquiry",
                    ""
                  );
                }}
              >
                {item.data.emailAddresses}
              </Text>
              <Text style={{ fontSize: 16, color: "#79991B", marginTop: 15 }}>
                Office Number
              </Text>
              <Text
                style={{ fontSize: 16, color: "#204F79", marginTop: 2 }}
                onPress={() => {
                  Communications.phonecall(
                    this.removeCodes(item.data.officeTelephone),
                    false
                  );
                }}
              >
                {this.removeCodes(item.data.officeTelephone)}
              </Text>
              <Text style={{ fontSize: 16, color: "#79991B", marginTop: 15 }}>
                Contact Press Office
              </Text>
              <Text
                style={{ fontSize: 16, color: "#204F79", marginTop: 2 }}
                onPress={() => {
                  Communications.phonecall(
                    this.removeCodes(item.data.alternativeOfficeTelephone),
                    false
                  );
                }}
              >
                {this.removeCodes(item.data.alternativeOfficeTelephone)}
              </Text>
              <Text style={{ fontSize: 16, color: "#79991B", marginTop: 15 }}>
                Spoken Languages
              </Text>
              <Text style={{ fontSize: 16, marginTop: 3, color: "#555" }}>
                {item.data.languagesSpoken.map((language, index) => {
                  if (index + 1 === item.data.languagesSpoken.length) {
                    return language;
                  } else {
                    return `${language}, `;
                  }
                })}
              </Text>
            </View>
            <Button
              raised
              style={{
                flex: 1
              }}
              icon={{
                name: "phone",
                type: "font-awesome",
                color: "#fff",
                size: 16
              }}
              onPress={() => {
                Communications.phonecall(
                  this.removeCodes(item.data.officeTelephone),
                  false
                );
              }}
              title="Call Expert"
              buttonStyle={{
                backgroundColor: "#204F79"
              }}
              titleStyle={{
                color: "#fff",
                fontSize: 16
              }}
            />
          </View>
        ) : (
          <View style={views.card}>
            <View style={{ position: "absolute", right: 20, top: 10 }}>
              <Icon
                name="chevron-down"
                type="font-awesome"
                size={16}
                color="#E1E1E1"
              />
            </View>
            <FlatList
              data={item}
              renderItem={({ item }) => {
                if (item.type !== "input") {
                  return (
                    <View style={views[item.class]}>
                      <Text style={texts[item.class]}>
                        {this.trimIf(this.stripTags(item.value), 1000)}
                      </Text>
                    </View>
                  );
                }
              }}
              keyExtractor={(item, index) => `list-item-${index}`}
            />
          </View>
        )}
      </TouchableWithoutFeedback>
    );
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <View style={views.container}>
        <FlatList
          keyboardShouldPersistTaps="always"
          ListHeaderComponent={
            <View>
              {this.header()}
              {this.searchBox()}
              {this.searchButtons()}
              {this.state.showingFilters ? this.filters() : null}
              {this.showExpertsCount()}
              {this.state.firstShow && this.state.dataSource ? (
                <CommentsBox />
              ) : null}
              {this.state.firstShow ? this.introText() : null}
            </View>
          }
          onEndReached={() => {
            this.getMoreContent();
          }}
          onEndReachedThreshold={0.25}
          data={this.state.dataSource}
          renderItem={({ item }) => this.card(item)}
          ListEmptyComponent={
            <View
              style={{
                flex: 1,
                backgroundColor: "#fff",
                marginTop: 10,
                padding: 20
              }}
            >
              <Text style={{ color: "#79991B", fontSize: 16 }}>
                Sorry this search has not returned any results
              </Text>
              <Text style={{ color: "#555", marginTop: 10 }}>
                However, we may still be able to help you, please call the Press
                Office on{" "}
                <Text
                  style={{ fontSize: 16, color: "#204F79", marginTop: 2 }}
                  onPress={() => {
                    Communications.phonecall("02476524668", false);
                  }}
                >
                  +44 (0) 24 7652 4668
                </Text>{" "}
                or{" "}
                <Text
                  style={{ fontSize: 16, color: "#204F79", marginTop: 2 }}
                  onPress={() => {
                    Communications.phonecall("07785433155", false);
                  }}
                >
                  +44 (0) 7785 433155
                </Text>{" "}
                or email{" "}
                <Text
                  style={{ fontSize: 16, color: "#204F79", marginTop: 2 }}
                  onPress={() => {
                    Communications.email(
                      "press@warwick.ac.uk",
                      "",
                      "",
                      "Enquiry",
                      ""
                    );
                  }}
                >
                  press@warwick.ac.uk
                </Text>
              </Text>
            </View>
          }
          keyExtractor={(item, index) => `list-item-${index}`}
        />
      </View>
    );
  }
}

const views = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E1E1E1",
    //    alignItems: "center",
    justifyContent: "center",
    position: "relative",
    padding: 10
  },
  header: {
    flexDirection: "row",
    paddingTop: 40
  },
  searchBox: {
    backgroundColor: "#79991B",
    padding: 10
  },
  card: {
    flex: 1,
    padding: 10,
    marginTop: 10,
    backgroundColor: "#ffffff"
  },
  name: {
    paddingBottom: 5
  },
  department: {
    paddingBottom: 10
  }
});

const texts = StyleSheet.create({
  inputField: {
    color: "red"
  },
  name: {
    color: "#79991B",
    fontSize: 18
  },
  department: {
    color: "#555555",
    fontSize: 16
  },
  expertise: {
    color: "#999999",
    fontSize: 14
  }
});
